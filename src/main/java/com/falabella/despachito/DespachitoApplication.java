package com.falabella.despachito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class DespachitoApplication extends SpringBootServletInitializer {


	
	public static void main(String[] args) {
		SpringApplication.run(DespachitoApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
////		cargarDatos();
//
//	}


//	public void deleteAll() {
//		System.out.println("Deleting all records..");
//		repository.deleteAll();
//	}
//	
//	public void addSampleData() {
//		System.out.println("Adding sample data");
//		Usuario usuario=new Usuario();
//		usuario.setApeMaterno("Perez");
//		usuario.setApePaterno("Barrios");	
//		usuario.setNombres("Alfredo");
//		repository.save(usuario);
//		
//		Pedido p=new Pedido();
//		p.setDescripcion("juego de comedor");
//		p.setUsuario(usuario);
//		pedidoRepository.save(p);
//	}
//	
//	public void listAll() {
//		System.out.println("Listing sample data");
//		repository.findAll().forEach(u -> System.out.println(u));
//	}
//	
//	public void findFirst() {
//		System.out.println("Finding first by Name");
//		Usuario u = repository.findFirstByNombres("Alfredo");
//		System.out.println(u);
//	}
	
//	public void findByRegex() {
//		System.out.println("Finding by Regex - All with address starting with ^New");
//		repository.findCustomByRegExAddress("^New").forEach(u -> System.out.println(u));
//	}
}
