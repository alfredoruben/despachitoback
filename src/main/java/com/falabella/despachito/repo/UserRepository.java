package com.falabella.despachito.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.falabella.despachito.domain.Usuario;

public interface UserRepository extends MongoRepository<Usuario, String> {
	
	Usuario findFirstByNombres(String nombres);
	Usuario findFirstByDni(String dni);
	
//    @Query("{address:'?0'}")
//    List<Usuario> findCustomByAddress(String address);
//
//    @Query("{address : { $regex: ?0 } }")
//    List<Usuario> findCustomByRegExAddress(String domain);

}
