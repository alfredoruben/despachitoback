package com.falabella.despachito.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.falabella.despachito.domain.Pedido;
import com.falabella.despachito.domain.Usuario;

public interface PedidoRepository extends MongoRepository<Pedido, String> {	
	List<Pedido> findFirstByUsuario(Usuario usuario);
	public List<Pedido> findByEstadoNot(String estado);
	public List<Pedido> findByEstado(String estado);
}
