package com.falabella.despachito.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.falabella.despachito.domain.Producto;

public interface ProductoRepository extends MongoRepository<Producto, String> {	
	Producto findFirstByNombre(String nombre);
}
