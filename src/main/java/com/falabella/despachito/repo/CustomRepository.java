package com.falabella.despachito.repo;

public interface CustomRepository {
	long updateUser(String address, Double salary);
}
