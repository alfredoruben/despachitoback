package com.falabella.despachito.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.falabella.despachito.domain.Empresa;

public interface EmpresaRepository extends MongoRepository<Empresa, String> {	
	Empresa findFirstByNombre(String nombre);
	
}
