package com.falabella.despachito.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.falabella.despachito.domain.Pedido;
import com.falabella.despachito.repo.PedidoRepository;
import com.falabella.despachito.repo.UserRepository;

@RestController
@RequestMapping("/pedidos")
public class PedidoController {
	 @Autowired
	 private PedidoRepository pedidoRepository;
	 
	 @Autowired
	 private UserRepository userRepository;
	 
	 @GetMapping
	 public List<Pedido> getAllPedidos() {
	   return pedidoRepository.findAll();
	 }
	
	 
	 @GetMapping("/id/{id}")
	 public Pedido getPedidoById(@PathVariable String id) {
		 return pedidoRepository.findById(id).get();
	 }
	 @GetMapping("/historial/{dni}")
	 public List<Pedido> getPedidoHistorial(@PathVariable String dni) {
		 return pedidoRepository.findByEstado("Entregado");
	 }
	 @GetMapping("/activos/{dni}")
	 public List<Pedido> getPedidoActivos(@PathVariable String dni) {
		 return pedidoRepository.findByEstadoNot("Entregado");
	 }
}
