package com.falabella.despachito.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.falabella.despachito.domain.Empresa;
import com.falabella.despachito.domain.Pedido;
import com.falabella.despachito.domain.Producto;
import com.falabella.despachito.domain.Usuario;
import com.falabella.despachito.repo.EmpresaRepository;
import com.falabella.despachito.repo.PedidoRepository;
import com.falabella.despachito.repo.ProductoRepository;
import com.falabella.despachito.repo.UserRepository;
import com.falabella.despachito.util.Util;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	 
		@Autowired
		UserRepository userRepository;
	//	
		@Autowired
		PedidoRepository pedidoRepository;
	//	
		@Autowired
		EmpresaRepository empresaRepository;
		
		@Autowired
		ProductoRepository productoRepository;
	 
	 @GetMapping
	 public List<Usuario> getAllUsuarios() {
	   return userRepository.findAll();
	 }
	 @GetMapping("/hello")
	 public String hello() {
		 
//		 cargarDatos();
		 
		 actualizar();
		 
	     return "hello azure!";
	 }
	 public void actualizar() {
		    Pedido pedido=pedidoRepository.findById("5ca84e09eab1402b7069f038").get();		    
			Producto producto=productoRepository.findById("5ca84e04eab1402b7069f037").get();//			
			Usuario usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			Empresa empresa=empresaRepository.findById("5ca84df5eab1402b7069f036").get();			
			pedido.setUsuario(usuario);
			pedido.setEmpresa(empresa);
			pedido.setProducto(producto);
			pedidoRepository.save(pedido);
			
			
			pedido=pedidoRepository.findById("5ca84e16eab1402b7069f03b").get();		    
			 producto=productoRepository.findById("5ca84e13eab1402b7069f03a").get();//			
			 usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			 empresa=empresaRepository.findById("5ca84e11eab1402b7069f039").get();			
			pedido.setUsuario(usuario);
			pedido.setEmpresa(empresa);
			pedido.setProducto(producto);
			pedidoRepository.save(pedido);
			
			pedido=pedidoRepository.findById("5ca84e23eab1402b7069f03d").get();		    
			 producto=productoRepository.findById("5ca84e19eab1402b7069f03c").get();//			
			 usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			 empresa=empresaRepository.findById("5ca84e11eab1402b7069f039").get();			
			pedido.setUsuario(usuario);
			pedido.setEmpresa(empresa);
			pedido.setProducto(producto);
			pedidoRepository.save(pedido);
			
			pedido=pedidoRepository.findById("5ca84e2deab1402b7069f040").get();		    
			 producto=productoRepository.findById("5ca84e26eab1402b7069f03f").get();//			
			 usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			 empresa=empresaRepository.findById("5ca84e24eab1402b7069f03e").get();			
			pedido.setUsuario(usuario);
			pedido.setEmpresa(empresa);
			pedido.setProducto(producto);
			pedidoRepository.save(pedido);
			
			pedido=pedidoRepository.findById("5ca8c7a90a803ab6a060141a").get();		    
			 producto=productoRepository.findById("5ca8d355eab140ca2c84c6d6").get();//			
			 usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			 empresa=empresaRepository.findById("5ca84e11eab1402b7069f039").get();			
			pedido.setUsuario(usuario);
			pedido.setEmpresa(empresa);
			pedido.setProducto(producto);
			pedidoRepository.save(pedido);
			
			pedido=pedidoRepository.findById("5ca8d0b8eab140bf6c996a3b").get();		    
			 producto=productoRepository.findById("5ca8d0b8eab140bf6c996a3a").get();//			
			 usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			 empresa=empresaRepository.findById("5ca84df5eab1402b7069f036").get();			
			pedido.setUsuario(usuario);
			pedido.setEmpresa(empresa);
			pedido.setProducto(producto);
			pedidoRepository.save(pedido);
		 
		 
	 }
		public void cargarDatos() {
//			Empresa empSaga=new Empresa();
//			empSaga.setNombre("Saga");
//			empresaRepository.save(empSaga);
//
//			
//			Usuario usuario=new Usuario();
//			usuario.setApeMaterno("Perez");
//			usuario.setApePaterno("Barrios");	
//			usuario.setNombres("Alfredo");
//			usuario.setDni("43547663");
//			userRepository.save(usuario);
//			
//			Producto producto=new Producto();
//			producto.setNombre("Televisor 48 pulg");
//			productoRepository.save(producto);
//			
//			Pedido pedido=new Pedido();
//			pedido.setUsuario(usuario);
//			pedido.setDireccion("centenario 482");
//			pedido.setEmpresa(empSaga);
//			pedido.setEstado("compra realizada");
//			pedido.setProducto(producto);		
//			pedido.setFechaCompra(Util.fechaACadena(new Date()));
//			pedido.setFechaEntrega("10/04/2019");
//			pedidoRepository.save(pedido);
			Empresa empSaga=empresaRepository.findById("5ca84df5eab1402b7069f036").get();

			
			Usuario usuario=userRepository.findById("5ca83949eab14004a072ff0e").get();
			
			
			Producto producto=new Producto();
			producto.setNombre("iphone 6");
			productoRepository.save(producto);
			
			Pedido pedido=new Pedido();
			pedido.setUsuario(usuario);
			pedido.setDireccion("centenario 482");
			pedido.setEmpresa(empSaga);
			pedido.setEstado("En Delivery");
			pedido.setProducto(producto);		
			pedido.setFechaCompra(Util.fechaACadena(new Date()));
			pedido.setFechaEntrega("09/05/2019");
			pedidoRepository.save(pedido);
			
			
			
	}
}
